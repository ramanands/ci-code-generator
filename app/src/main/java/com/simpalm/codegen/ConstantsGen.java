package com.simpalm.codegen;

import static com.simpalm.codegen.CMSReaderWriter.appendToConstants;

public class ConstantsGen {
    public void generateConstants() {
        String fileName = "constants";

        appendToConstants(fileName, getPageSizeConstant());
        appendToConstants(fileName, getResponseConstants());
    }

    private String getPageSizeConstant() {
        return "\ndefined('DEFAULT_PAGE_SIZE')\tOR define('DEFAULT_PAGE_SIZE', 10); // max items in page";
    }

    private String getResponseConstants() {
        return "\ndefined('STATUS_OK')\t\t\tOR define('STATUS_OK', 'OK'); // OK\n" +
                "defined('STATUS_ERROR')\t\t\tOR define('STATUS_ERROR', 'ERROR'); // ERROR";
    }
}
