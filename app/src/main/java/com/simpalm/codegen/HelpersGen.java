package com.simpalm.codegen;

import static com.simpalm.codegen.CMSReaderWriter.appendToHelper;

public class HelpersGen {
    public static final String HELPER_NAME = "main_helper";

    public void generateHelper() {
        String fileName = HELPER_NAME;

        appendToHelper(fileName, getPHPHeader());
        appendToHelper(fileName, getEchoResponseMethod());
        appendToHelper(fileName, getPHPFooter());
    }

    private String getPHPHeader() {
        return "<?php\n";
    }

    private String getEchoResponseMethod() {
        return "\n" +
                "function echo_response($status = STATUS_OK, $message = '', $data = '[]') {\n" +
                "\t$response = array('status' => $status, 'message' => $message, 'data' => $data);\n" +
                "\t$response = json_encode($response);\n" +
                "\techo $response;\n" +
                "\texit();\n" +
                "}";
    }

    private String getPHPFooter() {
        return "\n\n" +
                "?>";
    }
}
