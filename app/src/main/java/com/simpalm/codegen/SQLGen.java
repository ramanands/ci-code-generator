package com.simpalm.codegen;

import static com.simpalm.codegen.CMSReaderWriter.appendToSql;

public class SQLGen {
    public void generateSql(String name, String[] fields) {
        String fileName = "main";
        String tableName = Util.plural(name);

        appendToSql(fileName, getSqlDropTable(tableName));
        appendToSql(fileName, getSqlTableComment(tableName));
        appendToSql(fileName, getSqlCreateTable(tableName, fields));
    }

    private String getSqlDropTable(String tableName) {
        return "\n\n\nDROP TABLE IF EXISTS `" + tableName + "`;";
    }

    private String getSqlTableComment(String tableName) {
        return "\n\n#\n" +
                "# Table structure for table '" + tableName + "'\n" +
                "#";
    }

    private String getSqlCreateTable(String tableName, String[] fields) {
        StringBuilder sb = new StringBuilder();
        if (fields != null) {
            for (String field : fields) {
                if (!Util.isCommonField(field)) {
                    sb.append("  `" + field + "` varchar(8) NOT NULL,\n");
                }
            }
        }
        return "\nCREATE TABLE `" + tableName + "` (\n" +
                "  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,\n" +
                sb.toString() +
                "  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
                "  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n" +
                "  PRIMARY KEY (`id`)\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    }
}
