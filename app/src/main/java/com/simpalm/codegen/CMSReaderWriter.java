package com.simpalm.codegen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CMSReaderWriter {
    private static final String PROJECT_DIR = "/Users/ram/Documents/CodeBase/EclipsePHP/bluechips/";
    private static final String APPLICATION_DIR = PROJECT_DIR + "application/";
    private static final String CONTROLLER_DIR = APPLICATION_DIR + "controllers";
    private static final String VIEWS_DIR = APPLICATION_DIR + "views";
    private static final String MODEL_DIR = APPLICATION_DIR + "models";
    private static final String HELPERS_DIR = APPLICATION_DIR + "helpers";
    private static final String SQL_DIR = APPLICATION_DIR + "deployment";
    private static final String CONSTANTS_DIR = APPLICATION_DIR + "config";
    private static final String PHP_EXT = ".php";
    private static final String SQL_EXT = ".sql";

    public static boolean appendToController(String name, String text) {
        return append(CONTROLLER_DIR, name + PHP_EXT, text);
    }

    public static boolean appendToModel(String name, String text) {
        return append(MODEL_DIR, name + PHP_EXT, text);
    }

    public static boolean appendToSql(String name, String text) {
        return append(SQL_DIR, name + SQL_EXT, text);
    }

    public static boolean appendToView(String name, String text) {
        return append(VIEWS_DIR, name + PHP_EXT, text);
    }

    public static boolean appendToHelper(String name, String text) {
        return append(HELPERS_DIR, name + PHP_EXT, text);
    }

    public static boolean appendToConstants(String name, String text) {
        return append(CONSTANTS_DIR, name + PHP_EXT, text);
    }

    public static boolean append(String directory, String fileName, String text) {
        File dir = new File(directory);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String absolutePath = directory + File.separator + fileName;

        try (FileWriter fileWriter = new FileWriter(absolutePath, true)) {
            fileWriter.write(text);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String read(String directory, String fileName) {
        String absolutePath = directory + File.separator + fileName;
        try (FileReader fileReader = new FileReader(absolutePath)) {
            BufferedReader br = new BufferedReader(fileReader);

            StringBuilder sb = new StringBuilder();
            String string;
            while ((string = br.readLine()) != null) {
                sb.append(string);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int totalCodeLines = 0, totalFilesCount = 0;

    public void countLines() {
        totalCodeLines = 0;
        totalFilesCount = 0;

        countLines(CONTROLLER_DIR);
        countLines(MODEL_DIR);
        countLines(VIEWS_DIR);
        countLines(VIEWS_DIR);

        System.out.println("totalFilesCount=" + totalFilesCount + ", totalCodeLines=" + totalCodeLines);
    }

    public void countLines(String directory) {
        File dir = new File(directory);
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                countLines(file.getAbsolutePath());
            } else {
                int count = 0;
                try (FileReader fileReader = new FileReader(file)) {
                    BufferedReader br = new BufferedReader(fileReader);
                    String string;
                    while ((string = br.readLine()) != null) {
                        if (string.trim().length() > 0) count++;
                    }
                    totalCodeLines += count;
                    totalFilesCount++;
                    System.out.println(count + " <=> " + file.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
