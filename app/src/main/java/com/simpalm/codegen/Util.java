package com.simpalm.codegen;

import java.util.HashMap;
import java.util.Map;

public class Util {
    public static final String AUTHOR_STR = "@author\tRam Singh (http://simpalm.com/)";

    public static String getNoScriptAllowed() {
        return "<?php if (!defined('BASEPATH')) exit('No direct script access allowed');";
    }

    public static String capitalise(String text) {
        if (text != null) {
            if (text.length() > 1) {
                return text.substring(0, 1).toUpperCase() + text.substring(1);
            } else if (text.length() == 1) {
                return text.substring(0, 1).toUpperCase();
            }
        }
        return text;
    }

    public static String capitaliseAll(String text) {
        if (text != null) {
            StringBuilder sb = new StringBuilder();
            String[] texts = text.split(" ");
            if (texts != null) {
                for (int i = 0; i < texts.length; i++) {
                    sb.append(capitalise(texts[i]));
                    if (i < texts.length - 1) {
                        sb.append(" ");
                    }
                }
            }
            return sb.toString();
        }
        return text;
    }

    public static String plural(String text) {
        final Map<String, String> pluralMap = new HashMap<>();
        pluralMap.put("search", "searches");
        pluralMap.put("category", "categories");
        if (text != null) {
            if (pluralMap.containsKey(text.toLowerCase())) {
                return pluralMap.get(text.toLowerCase());
            }
            return text + "s";
        }
        return text;
    }

    public static boolean isCommonField(String field) {
        String[] commonFields = new String[]{"id", "created", "updated"};
        for (String fld : commonFields) {
            if (fld.equalsIgnoreCase(field)) return true;
        }
        return false;
    }
}
