package com.simpalm.codegen;

import static com.simpalm.codegen.CMSReaderWriter.appendToView;

public class ViewGen {
    public void generateView(String name, String[] fields) {
        String className = Util.capitalise(name);
        String fileName = Util.plural(name);

        appendToView(fileName, getViewHtml(className, fileName, name, fields));
        appendToView(fileName, getViewJavascript(className, name));
        //appendToView(name + "_item", getViewItemHtml(className, fileName, name, fields));
        appendToView(name + "_add_edit", getViewAddEditHtml(className, name, fields));
        appendToView(name + "_add_edit", getViewAddEditJavascript(name));
    }

    private String getViewHtml(String className, String fileName, String modelName, String[] fields) {
        float widthPercent = (float) 100 / fields.length;
        StringBuilder tblTpSb = new StringBuilder();
        StringBuilder tblThSb = new StringBuilder();
        StringBuilder tblTdSb = new StringBuilder();
        if (fields != null) {
            for (String field : fields) {
                if (!Util.isCommonField(field)) {
                    tblTpSb.append("                <col width=\"" + widthPercent + "%\">\n");
                    tblThSb.append("                    <th>" + Util.capitaliseAll(field) + "</th>\n");
                    tblTdSb.append("                    <td><?php echo $" + modelName + "->" + field + "; ?></td>\n");
                }
            }
        }
        return "<div class=\"row\">\n" +
                "        <div style=\"padding: 30px 0 0 0; font-weight: 600; text-align: center\">\n" +
                "           Delete Account\n" +
                "        </div>\n" +
                "    </div>" +
                "\n\n<?php if(isset($message)) echo $message; ?>\n" +
                "\n" +
                "<div class=\"row\" style=\"margin: 1.5% auto;\">\n" +
                "            <div class=\"notification-row\">\n" +
                "            <div class=\"row\" style=\"position: relative; margin: 1% 0;\">\n" +
                "                <div class=\"form-heading\">" + Util.capitalise(fileName) + "</div>\n" +
                "                <a class=\"btn-form btn-gray btn-table-addnew\" id=\"btnAddAdmin\" style=\"float: right; padding: 1.2% 2%;\" href=\"<?php echo base_url(\"/index.php/"+modelName+"/addedit/\"); ?>\">Add New " + className + "</a>\n" +
                "            </div>\n" +
                "            <?php if(!empty($" + fileName + ")) { ?>\n" +
                "            <table id=\"t01\">\n" +
                tblTpSb.toString() +
                "                <tr>\n" +
                tblThSb.toString() +
                "                </tr>\n" +
                "                <?php foreach ($" + fileName + " as $" + modelName + ") { ?>\n" +
                "                <tr>\n" +
                tblTdSb.toString() +
                "                    <td>\n" +
                "                        <a class=\"btn-table-form btn-yellow\" href=\"<?php echo base_url(\"/index.php/" + modelName + "/addedit/$" + modelName + "->id\"); ?>\">Edit</a>\n" +
                "                        <a class=\"btn-table-form btn-red\" onclick=\"return delete" + className + "(this);\" href=\"<?php echo $" + modelName + "->id; ?>\">Delete</a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <?php } ?>\n" +
                "\n" +
                "            </table>\n" +
                "            <?php } else { ?>\n" +
                "            No " + fileName + " found.\n" +
                "            <?php } ?>\n" +
                "</div>\n" +
                "        </div>";
    }

    private String getViewJavascript(String className, String modelName) {
        return "\n\n\t<script>\n" +
                "    function delete" + className + "(obj) {\n" +
                "        if(confirm('Are you sure you want to delete the " + modelName + "?')) {\n" +
                "\t    \t$(obj).attr(\"disabled\",\"disabled\");\n" +
                "\t    \tvar id = $(obj).attr(\"href\");\n" +
                "\t    \t$.post(\"<?php echo base_url('/" + modelName + "/delete') ?>\", {id: id}, function(data, status) {\n" +
                "\t    \t\t$(obj).attr(\"disabled\",\"\");\n" +
                "\t        \tvar response = JSON.parse(data);\n" +
                "\t            if(response.status == 'OK'){\n" +
                "\t            \tlocation.reload();\n" +
                "\t            } else {\n" +
                "\t            \talert(response.message);\n" +
                "\t            }\n" +
                "\t        });\n" +
                "        }\n" +
                "        return false;\n" +
                "    } \n" +
                "    //load " + modelName + " through ajax\n" +
                "    var fetchingPage=false;\n" +
                "    var pageOffset = 10;\n" +
                "    $(window).scroll(function() {\n" +
                "        if(!fetchingPage && $(window).scrollTop() == $(document).height() - $(window).height()) {\n" +
                "        \tfetchingPage = true;\n" +
                "        \t$.get(\"<?php echo base_url('/" + modelName + "/index_items/') ?>\", {q_offset: pageOffset }, function(data, status) {\n" +
                "            \tfetchingPage = false;\n" +
                "\t        \tvar response = JSON.parse(data);\n" +
                "\t\t    \tif(response.status == 'OK'){\n" +
                "    \t    \t\tpageOffset += 10;\n" +
                "\t\t            $('#t01').append(response.data);\n" +
                "\t\t            fetchingPage = response.data.length == 0;//all items loaded\n" +
                "\t\t        } else {\n" +
                "\t\t            alert(response.message);\n" +
                "\t\t        }\n" +
                "        \t});\n" +
                "        }\n" +
                "    });\n" +
                "    </script>";
    }

    private String getViewItemHtml(String className, String fileName, String modelName, String[] fields) {
        StringBuilder tblTdSb = new StringBuilder();
        if (fields != null) {
            for (String field : fields) {
                if (!Util.isCommonField(field)) {
                    tblTdSb.append("\t\t<td><?php echo $" + modelName + "->" + field + "; ?></td>\n");
                }
            }
        }
        return "<?php foreach ($" + fileName + " as $" + modelName + ") { ?>\n" +
                "\t<tr>\n" +
                tblTdSb.toString() +
                "\t\t<td>\n" +
                "\t\t\t<a class=\"btn-table-form btn-yellow\" href=\"<?php echo base_url(\"/" + modelName + "/addedit/$" + modelName + "->id\"); ?>\">Edit</a>\n" +
                "\t\t\t<a class=\"btn-table-form btn-red\" onclick=\"return delete" + className + "(this);\" href=\"<?php echo $" + modelName + "->id; ?>\">Delete</a>\n" +
                "\t\t</td>\n" +
                "\t</tr>\n" +
                "<?php } ?>";
    }

    private String getViewAddEditHtml(String className, String modelName, String[] fields) {
        StringBuilder sb = new StringBuilder();
        if (fields != null) {
            for (String field : fields) {
                if (!Util.isCommonField(field)) {
                    sb.append("                    " + Util.capitaliseAll(field) + "\n" +
                            "                    <?php echo form_input($" + field + ");?>\n");
                }
            }
        }
        return "<?php if(isset($message)) echo $message; ?>\n" +
                "\n" +
                "<div class=\"row\" style=\"margin: 1.5% auto;\">\n" +
                "            <div class=\"notification-row\">\n" +
                "        <?php echo form_open_multipart($this->uri->uri_string(), array('id' => '" + modelName + "_form')); ?>\n" +
                "            <p class=\"form-heading\"><?php echo (empty($id) ? 'Add' : 'Edit'); ?> " + className + "</p>\n" +
                sb.toString() +
                "\n" +
                "            <div class=\"row\">\n" +
                "                <a class=\"btn-form btn-yellow\" onclick=\"return submitForm(this);\" href=\"#\">Submit</a>\n" +
                "                <a class=\"btn-form btn-red\" onclick=\"return goBack(this);\" href=\"#\">Cancel</a>\n" +
                "            </div>\n" +
                "\n" +
                "        <?php echo form_close();?>\n" +
                "</div>\n" +
                "        </div>";
    }

    private String getViewAddEditJavascript(String modelName) {
        return "\n    <script>\n" +
                "    function submitForm(obj) {\n" +
                "    \t$(\"#" + modelName + "_form\").submit();\n" +
                "        return false;\n" +
                "    }\n" +
                "    function goBack(obj) {\n" +
                "    \thistory.back();\n" +
                "        return false;\n" +
                "    } \n" +
                "    </script>";
    }
}
