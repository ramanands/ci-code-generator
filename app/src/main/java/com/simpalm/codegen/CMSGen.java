package com.simpalm.codegen;

public class CMSGen {

    public static void main(String[] args) {
        CMSGen gen = new CMSGen();

        /*final String[] modelDefStrings = new String[]{
                "student_education:student_id,title,gpa,year,location",
                "student_sport:student_id,sport_id,ppg,fgm,fga,fg_percent,ftm,fta,ft_percent,three_fgm,three_fga,three_fg_percent,rpg,apg,spg,bpg,tpg,travel_team,trainer_name,position,height,weight,arm_length",
                "student_score:student_id,act,sat,other",
                "achievement:user_id,text",
                "interest:user_id,text",
                "post_content:user_id,photo_path,video_path,text,comment_count,high_five_count,share_count",
                "post:user_id,post_id,post_type",
                "notification: from_user_id,to_user_id,related_id,notification_type",
                "user_connection:from_user_id,to_user_id,to_user_type,connection_type",
                "message:from_user_id,to_user_id,last_message",
                "sport:title",
                "sport_field:sport_id,title,group_title,validations",
                "sport_position:sport_id,title",
                "school_college:title,address,school_college_type",
                "fan_sport:user_id,sport_id",
                "coach_profile:user_id,school_jrsr_college,set_skills,featured_projects",
                "college_profile:user_id,title,founded_year,overview,address,zipcode,city,contact_number,website,email,office_hours",
                "scholarship:user_id,title,description,requirements,amount,deadline,photo"};*/
        //final String[] modelDefStrings = new String[]{"user:profile_photo,first_name,last_name,gender,bio,location"};
        final String[] modelDefStrings = new String[]{"movie:user_id,title,description,duration"};
        for (String modelDefStr : modelDefStrings) {
            String name = gen.getName(modelDefStr);
            String[] fields = gen.getFieldNames(modelDefStr);
            new ModelGen().generateModel(name, fields);
            new ControllerGen().generateController(name, fields);
            new SQLGen().generateSql(name, fields);
            new ViewGen().generateView(name, fields);
        }
        new HelpersGen().generateHelper();
        new ConstantsGen().generateConstants();
        System.out.println("Generation completed.");
    }

    private String getName(String modelDefStr) {
        String[] names = modelDefStr.split(":");
        if (names.length >= 2) return names[0];
        return null;
    }

    private String[] getFieldNames(String modelDefStr) {
        String[] names = modelDefStr.split(":");
        if (names.length >= 2) {
            String[] fields = names[1].split(",");
            for (int i=0; i<fields.length; i++) {
                fields[i] = fields[i].trim();
            }
            return fields;
        }
        return null;
    }
}
