package com.simpalm.codegen;

import static com.simpalm.codegen.CMSReaderWriter.appendToController;

public class ControllerGen {
    public void generateController(String name, String[] fields) {
        String className = Util.capitalise(name);
        String tableName = Util.plural(name);

        appendToController(className, Util.getNoScriptAllowed());
        appendToController(className, getControllerComment(className));
        appendToController(className, getControllerClassAndConstructor(className, name));
        appendToController(className, getControllerIndex(tableName, name));
        appendToController(className, getControllerIndexItems(className, tableName, name));
        appendToController(className, getControllerAddEdit(className, fields, name));
        appendToController(className, getControllerDelete(className, name));
        appendToController(className, getControllerEnd(className));
    }

    private String getControllerComment(String tableName) {
        return "\ndefine('PAGE_SIZE', DEFAULT_PAGE_SIZE);\n" +
                "\n/**\n" +
                " * Controller class for " + tableName + "\n" +
                " * \n" +
                " * " + Util.AUTHOR_STR + "\n" +
                " */";
    }

    private String getControllerClassAndConstructor(String className, String modelName) {
        return "\nclass " + className + " extends MY_Controller {\n" +
                "\t\n" +
                "\tpublic function __construct()\n" +
                "\t{\n" +
                "\t\tparent::__construct();\n" +
                "\t\t\n" +
                "\t\t$this->load->library('form_validation');\n" +
                "\t\t$this->load->helper('" + HelpersGen.HELPER_NAME + "');\n" +
                "\t\t$this->load->model('" + modelName + ModelGen.MODEL_SUFFIX + "');\n" +
                "\t}";
    }

    private String getControllerIndex(String tableName, String modelName) {
        return "\n\n\tpublic function index()\n" +
                "\t{\n" +
                "\t\t$this->data['" + tableName + "'] = $this->" + modelName + ModelGen.MODEL_SUFFIX + "->get(0, PAGE_SIZE);\n" +
                "\t\t$this->template('" + tableName + "', $this->data);\n" +
                "\t}";
    }

    private String getControllerIndexItems(String className, String tableName, String modelName) {
        return "\n\n\tpublic function index_items() {\n" +
                "\t\t$offset\t\t\t= $this->input->get('q_offset');\n" +
                "\t\tif(empty($offset)) {\n" +
                "\t\t\t$offset = 0;\n" +
                "\t\t}\n" +
                "\t\t\n" +
                "\t\t$this->data['" + tableName + "'] = $this->" + modelName + ModelGen.MODEL_SUFFIX + "->get($offset, PAGE_SIZE);\n" +
                "\t\t$html = $this->load->view('" + modelName + "_item', $this->data, TRUE);\n" +
                "\t\techo_response(STATUS_OK, '" + className + " listed successfully.', $html);\n" +
                "\t}";
    }

    private String getControllerAddEdit(String className, String[] fields, String modelName) {
        StringBuilder rulesSb = new StringBuilder();
        StringBuilder postDataSb = new StringBuilder();
        StringBuilder inputFieldsSb = new StringBuilder();
        if (fields != null) {
            for (String field : fields) {
                if (!Util.isCommonField(field)) {
                    rulesSb.append("\t\t$this->form_validation->set_rules('" + field + "', '" + Util.capitaliseAll(field) + "', 'trim|required|min_length[1]|max_length[4096]');\n");
                    postDataSb.append("\t\t\t\t\t'" + field + "' => $this->input->post('" + field + "'),\n");
                    inputFieldsSb.append("\t\t\t$this->data['" + field + "'] = [\n" +
                            "\t\t\t\t\t'name' => '" + field + "',\n" +
                            "\t\t\t\t\t'id' => '" + field + "',\n" +
                            "\t\t\t\t\t'type' => 'text',\n" +
                            "\t\t\t\t\t'required' => 'required',\n" +
                            "\t\t\t\t\t'class' => 'form-input-field',\n" +
                            "\t\t\t\t\t'value' => $this->form_validation->set_value('" + field + "', isset($" + modelName + "->" + field + ") ? $" + modelName + "->" + field + " : ''),\n" +
                            "\t\t\t];\n");
                }
            }
        }
        return "\n\n\tpublic function addedit($id = 0)\n" +
                "\t{\n" +
                "\t\t// validate form input\n" +
                rulesSb.toString() +
                "\t\t\n" +
                "\t\tif ($this->form_validation->run() === TRUE)\n" +
                "\t\t{\n" +
                "\t\t\t$data = [\n" +
                postDataSb.toString() +
                "\t\t\t];\n" +
                "\t\t}\n" +
                "\t\tif ($this->form_validation->run() === TRUE)\n" +
                "\t\t{\n" +
                "\t\t\tif(empty($id) ) {\n" +
                "\t\t\t\tif($this->" + modelName + ModelGen.MODEL_SUFFIX + "->create($data)) {\n" +
                "\t\t\t\t\t$this->session->set_flashdata('message', '" + className + " added successfully.');\n" +
                "\t\t\t\t} else {\n" +
                "\t\t\t\t\t$this->session->set_flashdata('message', 'Ops! something went wrong while adding " + modelName + ". Please try again.');\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\tredirect(\"/" + modelName + "/\", 'refresh');\n" +
                "\t\t\t}\n" +
                "\t\t\t\n" +
                "\t\t\tif(!empty($id)) {\n" +
                "\t\t\t\tif($this->" + modelName + ModelGen.MODEL_SUFFIX + "->update($id, $data)) {\n" +
                "\t\t\t\t\t$this->session->set_flashdata('message', '" + className + " updated successfully.');\n" +
                "\t\t\t\t\tredirect(\"/" + modelName + "/\", 'refresh');\n" +
                "\t\t\t\t} else {\n" +
                "\t\t\t\t\t$this->session->set_flashdata('message', 'Ops! something went wrong while updating " + modelName + ". Please try again.');\n" +
                "\t\t\t\t\tredirect(\"/" + modelName + "/addedit/$id\", 'refresh');\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t\telse\n" +
                "\t\t{\n" +
                "\t\t\tif(!empty($id)) {\n" +
                "\t\t\t\t$" + modelName + " = $this->" + modelName + ModelGen.MODEL_SUFFIX + "->get_by_id($id);\n" +
                "\t\t\t\t$this->data['" + modelName + "'] = $" + modelName + ";\n" +
                "\t\t\t}\n" +
                "\t\t\t$this->data['id'] = $id;\n" +
                "\t\t\t// set the flash data error message if there is one\n" +
                "\t\t\t$this->data['message'] = (validation_errors() ? validation_errors() : NULL);\n" +
                "\t\t\n" +
                inputFieldsSb.toString() +
                "\n" +
                "\t\t\t$this->template('" + modelName + "_add_edit', $this->data);\n" +
                "\t\t}\n" +
                "\t}";
    }

    private String getControllerDelete(String className, String modelName) {
        return "\n\n\tpublic function delete() {\n" +
                "\t\t$id = $this->input->post('id');\n" +
                "\t\tif(empty($id)) {\n" +
                "\t\t\techo_response(STATUS_ERROR, '" + className + " ID is required.');\n" +
                "\t\t} else {\n" +
                "\t\t\t$deleted = $this->" + modelName + ModelGen.MODEL_SUFFIX + "->delete_by_id($id);\n" +
                "\t\t\tif(!empty($deleted)) {\n" +
                "\t\t\t\techo_response(STATUS_OK, '" + className + " deleted successfully.');\n" +
                "\t\t\t} else {\n" +
                "\t\t\t\techo_response(STATUS_ERROR, 'Ops! something went wrong while deleting " + className + ". Please try again.');\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t}";
    }

    private String getControllerEnd(String className) {
        return "\n}\n" +
                "\n" +
                "/* End of file " + className + ".php */\n" +
                "/* Location: ./application/controllers/" + className + " */";
    }
}
