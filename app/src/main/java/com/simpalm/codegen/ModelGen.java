package com.simpalm.codegen;

import java.util.Arrays;

import static com.simpalm.codegen.CMSReaderWriter.appendToModel;

public class ModelGen {
    public static final String MODEL_SUFFIX = "_model";

    public void generateModel(String name, String[] fields) {
        String className = Util.capitalise(name);
        String tableName = Util.plural(name);
        String fileName = className + MODEL_SUFFIX;

        appendToModel(fileName, Util.getNoScriptAllowed());
        appendToModel(fileName, getModelComment(className, tableName, fields));
        appendToModel(fileName, getModelClassAndConstructor(className, tableName));
        appendToModel(fileName, getModelCreate());
        appendToModel(fileName, getModelUpdate(fields));
        appendToModel(fileName, getModelGetById());
        appendToModel(fileName, getModelDeleteById());
        appendToModel(fileName, getModelGet());
        appendToModel(fileName, getModelEnd(className));
    }

    private String getModelComment(String className, String tableName, String[] fields) {
        return "\n\n/**\n" +
                " * " + className + "\n" +
                " *\n" +
                " * This model represents " + tableName + " data. It operates the following table:\n" +
                " * - " + tableName + "\n" +
                " * \n" +
                " * Fields: " + Arrays.toString(fields) + "\n" +
                " *\n" +
                " * " + Util.AUTHOR_STR + "\n" +
                " */";
    }

    private String getModelClassAndConstructor(String className, String tableName) {
        return "\nclass " + className + MODEL_SUFFIX + " extends CI_Model\n" +
                "{\n" +
                "\tprivate $table_name\t\t\t= '" + tableName + "';\t\t\t// " + tableName + "\n" +
                "\n" +
                "\tfunction __construct()\n" +
                "\t{\n" +
                "\t\tparent::__construct();\n" +
                "\n" +
                "\t\t$this->load->database();\n" +
                "\t}";
    }

    private String getModelCreate() {
        return "\n\n\t/**\n" +
                "\t * Create new record\n" +
                "\t *\n" +
                "\t * @param\tarray\n" +
                "\t * @return\tarray\n" +
                "\t */\n" +
                "\tfunction create($data)\n" +
                "\t{\n" +
                "\t\t//$data['created'] = date('Y-m-d H:i:s');\n" +
                "\t\n" +
                "\t\tif ($this->db->insert($this->table_name, $data)) {\n" +
                "\t\t\treturn $this->db->insert_id();\n" +
                "\t\t}\n" +
                "\t\treturn NULL;\n" +
                "\t}";
    }

    private String getModelUpdate(String[] fields) {
        StringBuilder sb = new StringBuilder();
        if (fields != null) {
            for (String field : fields) {
                if (!Util.isCommonField(field)) {
                    sb.append("\t\tif (isset($data['" + field + "']))\t\t\t\t$this->db->set('" + field + "', $data['" + field + "']);\n");
                }
            }
        }
        return "\n\n\t/**\n" +
                "\t * Update record\n" +
                "\t *\n" +
                "\t * @param\tstring\n" +
                "\t * @param\tarray\n" +
                "\t * @return\tbool\n" +
                "\t */\n" +
                "\tfunction update($id, $data)\n" +
                "\t{\n" +
                sb.toString() +
                "\n" +
                "\t\t$this->db->where('id', $id);\n" +
                "\t\t$this->db->update($this->table_name);\n" +
                "\t\treturn $this->db->affected_rows() > 0 || TRUE;//sometimes data remains same\n" +
                "\t}";
    }

    private String getModelGetById() {
        return "\n\n\t/**\n" +
                "\t * Get record by id.\n" +
                "\t *\n" +
                "\t * @param number $id\n" +
                "\t */\n" +
                "\tfunction get_by_id($id)\n" +
                "\t{\n" +
                "\t\t$this->db->where('id', $id);\n" +
                "\t\n" +
                "\t\t$query = $this->db->get($this->table_name);\n" +
                "\t\tif ($query->num_rows() == 1) return $query->row();\n" +
                "\t\treturn NULL;\n" +
                "\t}";
    }

    private String getModelDeleteById() {
        return "\n\n\t/**\n" +
                "\t * Delete a record by id.\n" +
                "\t *\n" +
                "\t * @param number $id\n" +
                "\t */\n" +
                "\tfunction delete_by_id($id)\n" +
                "\t{\n" +
                "\t\t$this->db->where('id', $id);\n" +
                "\t\t$this->db->delete($this->table_name);\n" +
                "\t\treturn $this->db->affected_rows() > 0;\n" +
                "\t}";
    }

    private String getModelGet() {
        return "\n\n\t/**\n" +
                "\t * Get all records.\n" +
                "\t *\n" +
                "\t * @param number $offset\n" +
                "\t * @param number $limit\n" +
                "\t */\n" +
                "\tfunction get($offset = 0, $limit = 1000)\n" +
                "\t{\n" +
                "\t\t$this->db->select(\"*\");\n" +
                "\t\t$this->db->from($this->table_name);\n" +
                "\t\n" +
                "\t\t$this->db->order_by('id', 'asc');\n" +
                "\t\n" +
                "\t\t$this->db->limit($limit, $offset);\n" +
                "\t\t$query = $this->db->get();\n" +
                "\t\t$result = $query->result_object();\n" +
                "\t\treturn $result;\n" +
                "\t}";
    }

    private String getModelEnd(String className) {
        return "\n}\n" +
                "\n" +
                "/* End of file " + className + ".php */\n" +
                "/* Location: ./application/models/" + className + " */";
    }
}
